const sarcastify = require('./sarcastify');

const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

function askAboutFood() {
  readline.question("What's your favorite food? ", food => {
    console.log(`Oh, so it's ${sarcastify(food)}, is it?`);
    askAboutFood();
  });
}

askAboutFood();