# Example: unit testing

Steps 1 through 3 are minimum requirements; steps 4 through 6 are for if you want to delve into actual test-driven development (TDD).

1. **`npm init`.**
2. **Install a testing framework.** You can use anything you want. I used Jest here. `npm i --save-dev jest`.
3. **Configure Node.js to look for the tests.** Put `"scripts": { "test": "jest" }` in your package.json.
4. **Write some tests.**
5. **Run the tests to make sure they fail.** `npm test`.
6. **Write the business logic to fulfill the tests.**