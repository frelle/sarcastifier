const sarcastify = require("./sarcastify");

test("Food items should sarcastify correctly.", () => {
  expect(sarcastify("lasagna")).toBe("lAsAgNa");
});

test("Empty strings should yield empty strings.", () => {
  expect(sarcastify("")).toBe("");
});

test("Numbers and special chars should be left unchanged.", () => {
  expect(sarcastify("123#$%beef")).toBe("123#$%bEeF");
});