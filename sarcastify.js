function sarcastify(s) {
  return [...s].map((c, i) => {
    if (i % 2 == 0) {
      return c.toLowerCase();
    } else {
      return c.toUpperCase();
    }
  }).join("");
}

module.exports = sarcastify;